﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using PhantomLib.Communication.Message;
using PhantomService.ServiceTask;

namespace PhantomService
{
    public enum ServiceState 
    {
        RUNNING,
        FAULTED
    }

    public delegate void RegisterMessage(MessageWrapper wrapper);

    public class ServiceManager
    {
        private CancellationTokenSource cancellationTokenSource;
        private ServiceState state;

        private Server server;

        private Task serverTask;

        public ServiceManager()
        {
            RegisterMessage registerMessage = this.handleMessage;

            server = new Server(ref registerMessage);
        }

        public async Task Start()
        {
            if (cancellationTokenSource != null)
            {
                throw new InvalidOperationException("Tasks are already running");
            }
            
            state = ServiceState.RUNNING;
            cancellationTokenSource = new CancellationTokenSource();
            
            serverTask = Task.Factory.StartNew(() => server.Receive(cancellationTokenSource.Token), cancellationTokenSource.Token);
            
            try
            {
                await serverTask;
            }
            catch
            {
                state = ServiceState.FAULTED;
                throw;
            }
        }

        public void Stop()
        {
            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();
                cancellationTokenSource = null;
            }
        }

        public void handleMessage(MessageWrapper wrapper)
        {
            File.Create("D:\\msg\\" + wrapper.Type);
        }
    }
}
