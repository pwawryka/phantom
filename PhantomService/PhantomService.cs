﻿using System;
using System.ComponentModel;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace PhantomService
{
    public class PhantomService : ServiceBase
    {
        private ServiceManager serviceManager;

        public PhantomService()
        {
            InitializeComponent();

            serviceManager = new ServiceManager();
        }

        public void OnDebug(string[] args)
        {
            OnStart(args);
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            serviceManager.Start();
            File.Create("D:\\chuj.txt");
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            serviceManager.Stop();
            File.Create("D:\\chujstop.txt");
            base.OnStop();
        }

        private void InitializeComponent()
        {
            this.AutoLog = true;
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;
            this.ServiceName = "PhantomService";
        }
    }
}
