﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace PhantomService
{
    [RunInstaller(true)]
    public class PhantomInstaller : Installer
    {
        private IContainer components = null;
        private ServiceProcessInstaller serviceProcessInstaller;
        private ServiceInstaller serviceInstaller;

        public PhantomInstaller()
        {
            serviceProcessInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            serviceProcessInstaller.Password = null;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            
            serviceInstaller.ServiceName = "PhantomService";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            
            Installers.Add(serviceInstaller);
            Installers.Add(serviceProcessInstaller);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            components = new Container();
        }
    }
}
