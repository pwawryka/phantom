﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;

using PhantomLib.Communication.Message;

namespace PhantomService
{
    public class Server
    {
        private RegisterMessage registerMessage;

        private NamedPipeServerStream receiverStream;
        private NamedPipeServerStream senderStream;
        private IFormatter messageSerializer;

        public Server(ref RegisterMessage registerMessage) : base()
        {
            this.registerMessage = registerMessage;
            this.messageSerializer = new BinaryFormatter();
        }

        public void Receive(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                receiverStream = new NamedPipeServerStream("Phantom_Pipe", PipeDirection.In);
                receiverStream.WaitForConnection();
                try
                {
                    while (receiverStream.IsConnected)
                    {
                        lock (messageSerializer)
                        {
                            registerMessage((MessageWrapper)messageSerializer.Deserialize(receiverStream));
                        }
                    }
                }
                catch (Exception)
                {
                    receiverStream.Close();
                    receiverStream.Dispose();
                }
            }
        }

        public void Send(string pipeName, MessageWrapper wrapper)
        {
            senderStream = new NamedPipeServerStream(pipeName, PipeDirection.Out);
            senderStream.WaitForConnection();

            lock (messageSerializer)
            {
                messageSerializer.Serialize(senderStream, wrapper);
            }

            senderStream.Close();
            senderStream.Dispose();
        }
    }
}
