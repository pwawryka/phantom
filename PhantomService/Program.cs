﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace PhantomService
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            /*PhantomService service = new PhantomService();
            service.OnDebug(args);
            Console.ReadKey();
             */
            System.ServiceProcess.ServiceBase.Run(new PhantomService());
#endif
         }
    }
}
