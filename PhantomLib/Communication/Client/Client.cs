﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

using PhantomLib.Communication.Message;

namespace PhantomLib.Communication.Client
{
    public class Client : IClient
    {
        private NamedPipeClientStream receiveStream;
        private NamedPipeClientStream sendStream;
        private IFormatter messageSerializer;

        public Client()
        {
            messageSerializer = new BinaryFormatter();
        }

        public void Send(MessageWrapper wrapper)
        {
            lock (this)
            {
                sendStream = new NamedPipeClientStream(".", "Phantom_Pipe", PipeDirection.Out);
                sendStream.Connect(1000);

                lock (messageSerializer)
                {
                    messageSerializer.Serialize(sendStream, wrapper);
                }

                sendStream.Close();
                sendStream.Dispose();
            }
        }

        public MessageWrapper Receive(string pipeName)
        {
            MessageWrapper wrapper;
            lock (this)
            {
                receiveStream = new NamedPipeClientStream(".", pipeName, PipeDirection.In);
                receiveStream.Connect(1000);

                lock (messageSerializer)
                {
                    wrapper = (MessageWrapper)messageSerializer.Deserialize(receiveStream);
                }

                receiveStream.Close();
                receiveStream.Dispose();
            }

            return wrapper;
        }
    }
}
