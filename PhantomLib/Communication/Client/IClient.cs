﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PhantomLib.Communication.Message;

namespace PhantomLib.Communication.Client
{
    interface IClient
    {
        void Send(MessageWrapper wrapper);
        MessageWrapper Receive(string channelId);
    }
}
