﻿using System;

namespace PhantomLib.Communication.Message
{
    public enum MessageType
    {
        HELLO,
        REGISTER,
        LOGIN,
        ERROR,
        INFO,
        SHUTDOWN,
        PROCESS,
        HOST
    }

    [Serializable]
    public class MessageWrapper
    {
        public MessageType Type { get; set; }
        public string AuthKey { get; set; }
        public IMessage Message { get; set; }
    }

    public interface IMessage { }
}
